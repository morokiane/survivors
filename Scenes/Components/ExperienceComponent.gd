extends Node

@export_range(0, 1) var dropPercent: float = 0.5
@export var healthComponent: Node
@export var vailScene: PackedScene

func _ready() -> void:
	(healthComponent as HealthComponent).died.connect(OnDied)

func OnDied() -> void:
	if randf() > dropPercent:
		return

	if vailScene == null:
		return

	if !owner is Node2D:
		return

	var spawnPosition = (owner as Node2D).global_position
	var instance: Node2D = vailScene.instantiate()
	var entitieslayer = get_tree().get_first_node_in_group("entitieslayer")

	entitieslayer.add_child(instance)
	instance.global_position = spawnPosition
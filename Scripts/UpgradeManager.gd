extends Node

const upgradeScreen: PackedScene = preload("res://Scenes/HUD/UpgradeScreen.scn")

@export var xpManager: Node

var currentUpgrades: Dictionary = {}
var upgradePool: WeightedTable = WeightedTable.new()
var axeUpgrade = preload("res://Resources/Axe.tres")
var axeDamage = preload("res://Resources/AxeDamage.tres")
var swordRate = preload("res://Resources/SwordRate.tres")
var swordDamage = preload("res://Resources/SwordDamage.tres")

func _ready() -> void:
	upgradePool.AddItem(axeUpgrade, 10)
	upgradePool.AddItem(swordRate, 10)
	upgradePool.AddItem(swordDamage, 10)

	xpManager.levelUp.connect(_on_level_up)

func ApplyUpgrade(upgrade: AbilityUpgrade) -> void:
	var hasUpgrade = currentUpgrades.has(upgrade.id)

	if !hasUpgrade:
		currentUpgrades[upgrade.id] = {
			"resource": upgrade,
			"quantity": 1
		}
	else:
		currentUpgrades[upgrade.id]["quantity"] += 1

	if upgrade.maxQuantity > 0:
		var currentQuantity = currentUpgrades[upgrade.id]["quantity"]
		if currentQuantity == upgrade.maxQuantity:
			upgradePool.RemoveItem(upgrade)
	
	UpdatePool(upgrade)
	GameController.emit_ability_upgrade_added(upgrade, currentUpgrades)

func UpdatePool(chosenUpgrade: AbilityUpgrade):
	if chosenUpgrade.id == axeUpgrade.id:
		upgradePool.AddItem(axeDamage, 10)

func PickUpgrades() -> Array:
	var chosenUpgrades: Array[AbilityUpgrade] = []

	for i in 2:
		if upgradePool.items.size() == chosenUpgrades.size():
			break

		var chosenUpgrade = upgradePool.PickItem(chosenUpgrades)

		chosenUpgrades.append(chosenUpgrade)

	return chosenUpgrades

func _on_upgrade_selected(upgrade: AbilityUpgrade) -> void:
	ApplyUpgrade(upgrade)

func _on_level_up(_currentLevel: int) -> void:
	var instance = upgradeScreen.instantiate()
	add_child(instance)
	var chosenUpgrades = PickUpgrades()
	instance.SetAbilityUpgrades(chosenUpgrades as Array[AbilityUpgrade])
	instance.upgrade_selected.connect(_on_upgrade_selected)

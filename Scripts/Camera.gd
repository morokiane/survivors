extends Camera2D

var targetPosition: Vector2 = Vector2.ZERO

func _ready() -> void:
	make_current()

func _process(delta: float) -> void:
	AquireTarget()
	global_position = global_position.lerp(targetPosition, 1.0 - exp(-delta * 20))

func AquireTarget() -> void:
	var playerNodes:= get_tree().get_nodes_in_group("player")

	if playerNodes.size() > 0:
		var player: Node2D = playerNodes[0]
		targetPosition = player.global_position

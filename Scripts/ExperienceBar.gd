extends CanvasLayer

@onready var progress = $MarginContainer/ProgressBar

@export var xpManager: Node

func _ready() -> void:
	progress.value = 0
	xpManager.xp_updated.connect(_on_xp_updated)

func _on_xp_updated(currentXP: float, targetXP: float) -> void:
	var percent = currentXP / targetXP
	progress.value = percent
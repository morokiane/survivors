extends Node

const spawnRadius: int = 375

@onready var timer: Timer = $Timer

@export var basicEnemy: PackedScene
@export var wizardEnemy: PackedScene
@export var arenaTimeManager: Node

var baseSpawnTime: float = 0
var enemyTable = WeightedTable.new()

func _ready() -> void:
	enemyTable.AddItem(basicEnemy, 10)
	baseSpawnTime = timer.wait_time
	arenaTimeManager.arena_difficulty_increase.connect(_on_arena_difficulty_increase)

func GetSpawnPosition() -> Vector2:
	var player: Node2D = get_tree().get_first_node_in_group("player")
	
	if player == null:
		return Vector2.ZERO

	var spawnPosition = Vector2.ZERO
	var randomDirection: Vector2 = Vector2.RIGHT.rotated(randf_range(0, TAU)) #tau = 2 * PI
	
	for i in 4:
		spawnPosition = player.global_position + (randomDirection * spawnRadius)

		var queryParameters = PhysicsRayQueryParameters2D.create(player.global_position, spawnPosition, 1)
		var result = get_tree().root.world_2d.direct_space_state.intersect_ray(queryParameters)

		if result.is_empty():
			break
			# return spawnPosition
		else:
			randomDirection = randomDirection.rotated(deg_to_rad(90))

	return spawnPosition
	
func _on_timer_timeout() -> void:
	timer.start()

	var player: Node2D = get_tree().get_first_node_in_group("player")
	
	if player == null:
		return
	
	var enemy = enemyTable.PickItem()
	var instance: Node2D = enemy.instantiate()
	var entitieslayer = get_tree().get_first_node_in_group("entitieslayer")

	entitieslayer.add_child(instance)
	instance.global_position = GetSpawnPosition()

func _on_arena_difficulty_increase(arenaDifficulty: int) -> void:
	var timeOff = (0.1 / 12) * arenaDifficulty
	timeOff = min(timeOff, 0.7)
	timer.wait_time = baseSpawnTime - timeOff

	if arenaDifficulty == 6:
		enemyTable.AddItem(wizardEnemy, 20)
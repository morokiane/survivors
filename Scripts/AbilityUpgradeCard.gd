extends PanelContainer

signal selected

@onready var nameLabel: Label = $%NameLabel
@onready var descriptionLabel: Label = $%DescriptionLabel

func _ready() -> void:
	gui_input.connect(_on_gui_input)

func PlayIn(delay: float) -> void:
	modulate = Color.TRANSPARENT
	await get_tree().create_timer(delay).timeout
	$AnimationPlayer.play("in")

func SetAbilityUpgrades(upgrade: AbilityUpgrade):
	nameLabel.text = upgrade.name
	descriptionLabel.text = upgrade.description

func _on_gui_input(event: InputEvent) -> void:
	if event.is_action_pressed("leftclick"):
		selected.emit()

func _on_mouse_entered() -> void:
	var tween = create_tween()
	tween.set_parallel()
	tween.tween_property(self, "scale", Vector2(1.2, 1.2), 0.25).set_ease(Tween.EASE_IN).set_trans(Tween.TRANS_SPRING)
	# tween.tween_property(sprite, "scale", Vector2.ZERO, 0.15).set_delay(0.35)
	# scale = Vector2(1.2, 1.2)

func _on_mouse_exited() -> void:
	var tween = create_tween()
	tween.tween_property(self, "scale", Vector2(1.0, 1.0), 0.25).set_ease(Tween.EASE_IN).set_trans(Tween.TRANS_SINE)
	# scale = Vector2(1, 1)

extends Resource
class_name AbilityUpgrade

@export var id: String
@export var name: String
@export_multiline var description: String
## Zero is infinite
@export var maxQuantity: int
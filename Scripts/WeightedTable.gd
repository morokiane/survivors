class_name WeightedTable

var items: Array[Dictionary] = []
var weightSum: int = 0

func AddItem(item, weight: int) -> void:
	items.append({"item": item, "weight": weight})
	weightSum += weight

func RemoveItem(removeItem) -> void:
	items = items.filter(func (item): return item["item"] != removeItem)
	weightSum = 0
	for item in items:
		weightSum += item["weight"]

func PickItem(exclude: Array = []):
	var adjustedItems: Array[Dictionary] = items
	var adjustedWeightSum = weightSum
	if exclude.size() > 0:
		adjustedItems = []
		adjustedWeightSum = 0
		for item in items:
			if item["item"] in exclude:
				continue
			adjustedItems.append(item)
			adjustedWeightSum += item["weight"]

	var chosenWeight = randi_range(1, adjustedWeightSum)
	var interationSum: int = 0

	for item in adjustedItems:
		interationSum += item["weight"]
		if chosenWeight <= interationSum:
			return item["item"]
	
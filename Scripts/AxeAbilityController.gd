extends Node

@export var axeAbility: PackedScene

var baseDamage: int = 5
var damagePercent: float = 1

func _ready() -> void:
	GameController.ability_upgrade_added.connect(_on_ability_upgrade_added)

func _on_timer_timeout() -> void:
	var player: Node2D = get_tree().get_first_node_in_group("player")
	
	if player == null:
		return
	
	var foregroundlayer = get_tree().get_first_node_in_group("foregroundlayer")

	var instance: Node2D = axeAbility.instantiate()
	foregroundlayer.add_child(instance)
	instance.global_position = player.global_position
	instance.hitboxComponent.damage = baseDamage * damagePercent

func _on_ability_upgrade_added(upgrade: AbilityUpgrade, currentUpgrades: Dictionary):
	if upgrade.id == "axeDamage":
		damagePercent = 1 + (currentUpgrades["axeDamage"]["quantity"] * 0.1)
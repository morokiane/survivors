extends Node

const maxRange: float = 150

@onready var timer: Timer = $Timer

@export var swordAbility: PackedScene

var baseDamage: int = 5
var damagePercent: float = 1
var baseWaitTime: float

func _ready() -> void:
	baseWaitTime = timer.wait_time
	timer.timeout.connect(on_timer_timeout)
	GameController.ability_upgrade_added.connect(_on_ability_upgrade_added)

func on_timer_timeout() -> void:
	var player: Node2D = get_tree().get_first_node_in_group("player")
	
	if player == null:
		return
	
	var enemies: Array = get_tree().get_nodes_in_group("enemy")
	#This filters out enemies that are further than the max range so the ability doesn't spawn on them
	enemies = enemies.filter(func(enemy: Node2D):
		return enemy.global_position.distance_squared_to(player.global_position) < pow(maxRange, 2)
	)
	
	if enemies.size() == 0:
		return

	enemies.sort_custom(func(a: Node2D, b: Node2D):
		var aDistance = a.global_position.distance_squared_to(player.global_position)
		var bDistance = b.global_position.distance_squared_to(player.global_position)
		return aDistance < bDistance
	)
	
	var instance: Node2D = swordAbility.instantiate() as SwordAbility
	var foregroundlayer = get_tree().get_first_node_in_group("foregroundlayer")
	foregroundlayer.add_child(instance)
	instance.hitboxComponent.damage = baseDamage * damagePercent

	instance.global_position = enemies[0].global_position
	instance.global_position += Vector2.RIGHT.rotated(randf_range(0, 2 * PI)) * 4

	var enemyDirection: Vector2 = enemies[0].global_position - instance.global_position
	instance.rotation = enemyDirection.angle()

func _on_ability_upgrade_added(upgrade: AbilityUpgrade, currentUpgrades: Dictionary):
	if upgrade.id == "swordRate":
		var percentReduction = currentUpgrades["swordRate"]["quantity"] * 0.1
		timer.wait_time = baseWaitTime * (1 - percentReduction)
		timer.start()
	elif upgrade.id == "swordDamage":
		damagePercent = 1 + (currentUpgrades["swordDamage"]["quantity"] * 0.15)
extends CanvasLayer

@onready var label = %Label
@export var arenaTimeManager: Node

func _process(_delta: float) -> void:
	if arenaTimeManager == null:
		return

	var timeElapsed = arenaTimeManager.GetTimeElapsed()
	label.text = FormatTime(timeElapsed)

func FormatTime(seconds: float) -> String:
	var minutes = floor(seconds / 60)
	var remaining = seconds - (minutes * 60)
	
	return ("%02d" % floor(minutes)) + ":" + ("%02d" % floor(remaining))
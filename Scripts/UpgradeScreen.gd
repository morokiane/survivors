extends CanvasLayer

signal upgrade_selected(upgrade: AbilityUpgrade)

@onready var cardContainer: HBoxContainer = $%CardContainer

@export var upgradeCardScene: PackedScene

func _ready() -> void:
	get_tree().paused = true

func SetAbilityUpgrades(upgrades: Array[AbilityUpgrade]) -> void:
	var delay: float = 0
	for upgrade in upgrades:
		var instance = upgradeCardScene.instantiate()
		cardContainer.add_child(instance)
		instance.SetAbilityUpgrades(upgrade)
		instance.PlayIn(delay)
		instance.selected.connect(_on_upgrade_selected.bind(upgrade))
		delay += 0.2

func _on_upgrade_selected(upgrade: AbilityUpgrade) -> void:
	upgrade_selected.emit(upgrade)
	get_tree().paused = false
	queue_free()
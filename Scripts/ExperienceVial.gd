extends Node2D

@onready var collision: CollisionShape2D = $Area2D/CollisionShape2D
@onready var sprite: Sprite2D = $Sprite2D

func TweenCollect(percent: float, startPosition: Vector2) -> void:
	var player = get_tree().get_first_node_in_group("player")
	if player == null:
		return

	global_position = startPosition.lerp(player.global_position, percent)
	var directionStart = player.global_position - startPosition

	var targetRotation = directionStart.angle() + deg_to_rad(90)
	rotation = lerp_angle(rotation, targetRotation, 1 - exp(-2 * get_process_delta_time()))

func Collect() -> void:
	GameController.EmitExperienceVial(1)
	queue_free()

func DisableCollision() -> void:
	collision.disabled = true

func _on_area_2d_area_entered(_area:Area2D) -> void:
	Callable(DisableCollision).call_deferred()

	var tween = create_tween()
	tween.set_parallel()
	tween.tween_method(TweenCollect.bind(global_position), 0.0, 1.0, 0.5).set_ease(Tween.EASE_IN).set_trans(Tween.TRANS_BACK)
	tween.tween_property(sprite, "scale", Vector2.ZERO, 0.15).set_delay(0.35)
	tween.chain() #this causes the tween to wait for previous to finish before moving past
	tween.tween_callback(Collect)
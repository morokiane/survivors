extends CharacterBody2D

@onready var damageInterval: Timer = $DamageInterval
@onready var healthComponent: Node = $HealthComponent
@onready var progressbar: ProgressBar = $ProgressBar
@onready var abilities = $Abilities
@onready var anim: AnimationPlayer = $AnimationPlayer
@onready var sprite: Sprite2D = $Sprite2D

var maxSpeed: float = 125
var accel: float = 25
var numberColliding: int = 0

func _ready() -> void:
	$CollisionArea.body_entered.connect(_on_body_entered)
	$CollisionArea.body_exited.connect(_on_body_exited)
	healthComponent.health_changed.connect(_on_health_changed)
	GameController.ability_upgrade_added.connect(_on_ability_upgrade_added)
	UpdateHealthHud()

func _physics_process(delta: float) -> void:
	var movementVector = Move()
	var direction = movementVector.normalized()
	var targetVelocity = direction * maxSpeed 

	velocity = velocity.lerp(targetVelocity, 1 - exp(-delta * accel))
	
	if movementVector.x != 0 || movementVector.y != 0:
		anim.play("walk")
	else:
		anim.stop()

	if movementVector.x == 1:
		sprite.flip_h = false
	else:
		sprite.flip_h = true

	move_and_slide()

func Move() -> Vector2:
	var xMovement: float = Input.get_action_strength("right") - Input.get_action_strength("left")
	var yMovement: float = Input.get_action_strength("down") - Input.get_action_strength("up")
		
	return Vector2(xMovement, yMovement)

func DealDamage() -> void:
	if numberColliding == 0 || !damageInterval.is_stopped():
		return

	healthComponent.Damage(1)
	damageInterval.start()

func UpdateHealthHud() -> void:
	progressbar.value = healthComponent.GetHealthPercent()

func _on_body_entered(_other_body: Node2D) -> void:
	numberColliding += 1
	DealDamage()

func _on_body_exited(_other_body: Node2D) -> void:
	numberColliding -= 1

func _on_damage_interval_timeout() -> void:
	DealDamage()
	
func _on_health_changed() -> void:
	UpdateHealthHud()

func _on_ability_upgrade_added(abilityUpgrade: AbilityUpgrade, _currentUpgrades: Dictionary) -> void:
	if !abilityUpgrade is Ability:
		return
	
	var ability = abilityUpgrade as Ability
	abilities.add_child(ability.abilityControllerScene.instantiate())

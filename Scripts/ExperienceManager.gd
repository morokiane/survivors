extends Node
signal xp_updated(currentxp: float, targetxp: float)
signal levelUp(newLevel: int)

const targetXPGrowth: float = 5

var currentXP: float = 0
var currentLevel: int = 1
var targetXP: float = 1

func _ready() -> void:
	GameController.experience_vial_collected.connect(ExperienceCollected)

func IncrementExperience(number: float) -> void:
	currentXP = min(currentXP + number, targetXP)
	xp_updated.emit(currentXP, targetXP)

	if currentXP == targetXP:
		currentLevel += 1
		targetXP += targetXPGrowth
		currentXP = 0
		xp_updated.emit(currentXP, targetXP)
		levelUp.emit(currentLevel)

func ExperienceCollected(number: float) -> void:
	IncrementExperience(number)
extends Node

signal arena_difficulty_increase(arenaDifficulty: int)

const difficultyInterval: int = 5

@onready var timer: Timer = $Timer

@export var end: PackedScene
# @export var totalTime: float

var arenaDifficulty: int = 0

func _ready() -> void:
	# Not sure why this is not working
	# timer.wait_time = totalTime
	timer.timeout.connect(_on_timer_timeout)

func _process(_delta: float) -> void:
	var nextTimeTarget = timer.wait_time - ((arenaDifficulty + 1) * difficultyInterval)
	if timer.time_left <= nextTimeTarget:
		arenaDifficulty += 1
		arena_difficulty_increase.emit(arenaDifficulty)

func GetTimeElapsed() -> float:
	return timer.wait_time - timer.time_left

func _on_timer_timeout() -> void:
	var instance = end.instantiate()
	add_child(instance)
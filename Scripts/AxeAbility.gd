extends Node2D

const maxRadius: float = 100

@onready var hitboxComponent: Area2D = $HitboxComponent

var baseRotation: Vector2 = Vector2.RIGHT

func _ready() -> void:
	baseRotation = Vector2.RIGHT.rotated(randf_range(0, TAU))
	var tween = create_tween()
	tween.tween_method(SpinAxe, 0.0, 2.0, 3.0)
	tween.tween_callback(queue_free)

func SpinAxe(rotations: float) -> void:
	var currentRadius: float = (rotations / 2) * maxRadius
	var currentDirection: Vector2 = baseRotation.rotated(rotations * (2 * PI))
	var player: Node2D = get_tree().get_first_node_in_group("player")
	
	if player == null:
		return

	global_position = player.global_position + (currentDirection * currentRadius)

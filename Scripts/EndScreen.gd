extends CanvasLayer

@onready var titleLabel: Label = $MarginContainer/PanelContainer/MarginContainer/VBoxContainer/TitleLabel
@onready var descriptionLabel: Label = $MarginContainer/PanelContainer/MarginContainer/VBoxContainer/DescriptionLabel

func _ready() -> void:
	get_tree().paused = true
	$%RestartBTN.pressed.connect(_on_restart_pressed)
	$%QuitBTN.pressed.connect(_on_quit_pressed)

func Defeat() -> void:
	titleLabel.text = "Defeated!"
	descriptionLabel.text = "You lost loser!"

func _on_restart_pressed() -> void:
	get_tree().paused = false
	get_tree().change_scene_to_file("res://Scenes/Levels/Main.scn")

func _on_quit_pressed() -> void:
	get_tree().quit()

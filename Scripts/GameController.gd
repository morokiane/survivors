extends Node

signal experience_vial_collected(number: float)
signal ability_upgrade_added(upgrade: AbilityUpgrade, currentUpgrades: Dictionary)

func _unhandled_input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_cancel"):
		get_tree().quit()

func EmitExperienceVial(number: float) -> void:
	experience_vial_collected.emit(number)

func emit_ability_upgrade_added(upgrade: AbilityUpgrade, currentUpgrades: Dictionary):
	ability_upgrade_added.emit(upgrade, currentUpgrades)
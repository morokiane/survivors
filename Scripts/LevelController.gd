extends Node

@onready var player: CharacterBody2D = $Entities/Player

@export var end: PackedScene

func _ready() -> void:
	player.healthComponent.died.connect(_on_player_died)

func _on_player_died() -> void:
	var instance = end.instantiate()
	add_child(instance)
	instance.Defeat()
extends CharacterBody2D

# @onready var healthComponent: HealthComponent = $HealthComponent
@onready var sprite: Sprite2D = $Sprite2D
@onready var velocityComponent = $VelocityComponent

@export var alwaysMove: bool = true

var isMoving: bool = false

func _physics_process(_delta: float) -> void:
	if isMoving || alwaysMove:
		velocityComponent.AccelerateToPlayer()
	else:
		velocityComponent.Deaccelerate()

	velocityComponent.Move(self)

	if velocity.x > 0:
		sprite.flip_h = true
	else:
		sprite.flip_h = false

func FindPlayer() -> Vector2:
	var playerNode: Node2D = get_tree().get_first_node_in_group("player")

	if playerNode != null:
		return (playerNode.global_position - global_position).normalized()
	return Vector2.ZERO

func SetMoving(moving: bool) -> void:
	isMoving = moving